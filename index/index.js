let BASE_URL = "http://localhost:3000/"

async function display_galleries()
{
	let response = await fetch('/gallery');
	let myJson = await response.json();
	let div = document.createElement("div");
	div.setAttribute("id", "all_galleries");
	div.innerHTML = '<h2>All Galleries</h2>'
	document.getElementById("galleries").appendChild(div);
	list_galleries(myJson, "all_galleries");
}

async function list_galleries(myJson, div_id)
{
	let ul = document.createElement("ul");
	for (let n in myJson.records)
	{
		let gallery = myJson.records[n];
		let li = document.createElement("li");
		li.setAttribute("id", gallery.id);

		// create a button when clicked lists objects
		let button = document.createElement("button");
		button.innerHTML = gallery.name + " (id=" + gallery.id + ")";
		// style for galleries
		button.style.fontWeight = "bold";
		// set button so on click lists all the objects of that gallery
		button.onclick = async function() {
			let response = await fetch(`/gallery/${gallery.id}/objects`); //TODO change to search/object, params[gallery] = gallery.id
			let objects_json = await response.json();
			list_objects(objects_json, gallery.id);
		}

		li.appendChild(button);
		ul.appendChild(li);
	}
	document.getElementById(div_id).appendChild(ul);

	// link to more galleries
	if (myJson.info.page < myJson.info.pages)
	{
		params = {}
		params['page'] = myJson.info.page + 1;
		let nextJson = await async_fetch(create_server_url("gallery", params))

		let more_galleries = document.createElement("button");
		more_galleries.innerHTML = "View more galleries";

		more_galleries.onclick = function() {
			list_galleries(nextJson, "all_galleries");
		}
		document.getElementById(div_id).appendChild(more_galleries);
	}
}

async function list_objects(myJson, div_id)
{
	let ul = document.createElement("ul");
	// if no objects
	if (myJson.records.length == 0)
	{
		ul.innerHTML = "No objects";
	}

	// if there are objects, list them as buttons
	for (let n in myJson.records)
	{
		let object = myJson.records[n];
		let li = document.createElement("li");

		// create a button when clicked, gives object details
		let button = document.createElement("button");
		button.innerHTML = object.title;
		button.onclick = function() {
			let details_div = document.createElement("div");
			details_div.id = object.id;
			button.appendChild(details_div);
			list_object_details(object.id, object.id);
		}

		li.appendChild(button);
		ul.appendChild(li);
	}
	document.getElementById(div_id).appendChild(ul);

	// link to more objects
	if (myJson.info.page < myJson.info.pages)
	{
		let more_objects = document.createElement("button");
		more_objects.innerHTML = "View more objects";
		console.log(myJson.info.next)
		let nextJson = await async_fetch(myJson.info.next);
		more_objects.onclick = function() {
			list_objects(nextJson, div_id);
		}
		document.getElementById(div_id).appendChild(more_objects);
	}
}

// list the details of an object and put it in the given div_id
async function list_object_details(object_id, div_id)
{
	let div = document.getElementById(div_id);

 	let myJson = await async_fetch(`/details/object/${object_id}`);

 	// details to display
 	let details = ["title", "dated", "classification", "period", "department"]
 	// display details as ul
 	let ul = create_ul(myJson, details);
 	div.appendChild(ul);

 	// display images
 	let images = display_images(myJson.images);
 	div.appendChild(images);

	// create link to all details
	let details_link = document.createElement("a");
	details_link.href = `${BASE_URL}details/object/${object_id}`;
	details_link.innerHTML = "View All Details";
	div.appendChild(details_link);
}

async function display_search_history() {
	let response = await fetch('/history');
	let myJson = await response.json();
	document.write(buildList(myJson));
}


// -------------------------------------------- //
// SEARCH FUNCTIONS //
 async function objectSearch(form_data)
 {
 	let myJson = await async_fetch(create_server_url("search/object", form_data));
 	return myJson;
 }

async function personSearch(form_data)
{
	// TODO: make this cleaner
	/* really hacky. build a dict of params. Take only the first field value pair
	that is filled in from the form. need the query param to be the first in the dict
	so that it looks like q=field=value */
	let params = {};
	for (const [key, value] of Object.entries(form_data))
	{
		if (value != '')
		{
			params[key] = value;
			break;
		}
	}

	// let myJson = await async_fetch(create_search_url("search/person", {}, form_data));
 	let myJson = await async_fetch(create_server_url("search/person", params, true));
 	return myJson;
}

async function exhibitionSearch(form_data)
{
	/* really hacky. build a dict of params. Take only the first field value pair
	that is filled in from the form. need the query param to be the first in the dict
	so that it looks like q=field=value */
	/* distinction between q=field:value, and parameter=value*/
	let params = {};
	let q = false;
	if (form_data["title"])
	{
		params["title"] = form_data["title"];
		q = true;
	}
	else
	{
		params = Object.assign(form_data);
		q = false;
	}

 	let myJson = await async_fetch(create_server_url("search/exhibition", params, q));
 	return myJson;
}

async function publicationSearch(form_data)
{
	/* really hacky. build a dict of params. Take only the first field value pair
	that is filled in from the form. need the query param to be the first in the dict
	so that it looks like q=field=value */
	let params = {};
	for (const [key, value] of Object.entries(form_data))
	{
		if (value != '')
		{
			params[key] = value;
			break;
		}
	}

 	let myJson = await async_fetch(create_server_url("search/publication", params, true));
 	return myJson;
}


// ---------------------------------------------------
// HELPER FUNCTIONS
// creates an unordered list for given fields of JSON object
// @param: [list of Strings] details: a list of fields for the JSON object
// if details is [], make a list of all the key value pairs in the JSON
// @return ul
function create_ul(myJson, details=[])
{
	let ul = document.createElement("ul");
	for (let n in details)
	{
		let detail = details[n];
		let li = document.createElement("li");
		if (myJson[detail])
		{
			li.innerHTML = detail + ": " + myJson[detail];
			ul.appendChild(li);
		}
	}

	// list all the key values in the JSON
	if (details == [])
	{
		Object.keys(myJson).forEach(function(key)
		{
			li = document.createElement("li");
			li.innerHTML = key + ": " + myJson[key];
			ul.appendChild(li);
		})
	}
	return ul;
}

// create a div with the images as links to the full-sized image
// @param [JSON image attribute]
// @return [HTML div object]
 function display_images(images)
 {
 	let div = document.createElement("div");
 	if (!images || images.length == 0)
 	{
 		div.innerHTML = "No images";
 	}
 	for (let n in images)
 	{
 		let imageurl = images[n].iiifbaseuri + "/full/full/0/native.jpg";
 		// create an image as a link.
 		// when clicked, it takes you to the full size image
 		let a = document.createElement("a");
 		a.setAttribute("href", imageurl);
 		let img = document.createElement("img");
 		img.setAttribute("src", imageurl);
 		img.setAttribute("style", "width:50px;height:50px;");
 		img.setAttribute("class", "image");

 		a.appendChild(img);
		div.appendChild(a);
 	}
 	return div;
 }

// @return result of fetch(url)
async function async_fetch(url)
{
	let response = await fetch(url);
	let myJson = await response.json();
	return myJson;
}

// @param [String] my server route.
// @param [Dict] params: the query params
// @param [Bool] q: set to true for q=field=value format
// @return [URL] which can be passed to async_fetch (or fetch())
function create_server_url(route, params, q=false)
{
	// route?q=field=value
	// currently only handles one q=field:value
	if (q)
	{
		let url = new URL(BASE_URL + route + "?q=");
		Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
		let new_url = url.href.replace("&", "");
		return new_url;
	}
	else
	{
		let url = new URL(BASE_URL + route + "?");
		Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
		return url;
	}
}

// builds nested lists from JSON object
function buildList(data) {
	let html = '<div>'
	html += '<ul>'
	for(item in data)
	{
		html += '<li>';
		if(typeof(data[item]) === 'object')
		{
			html += item + ": "
			html += buildList(data[item]);
		}
		else
		{
			html += item + ": " + data[item];
		}
		html += "</li>"
	}
	html += "</ul>"
	return html;
}


function show_element(element_id)
{
 	document.getElementById(element_id).style.display = "initial";
}

function hide_element(element_id)
{
 	document.getElementById(element_id).style.display = "none";
}


//------------------------------------------------------//
// Search class
// TODO: finish moving resourceSearch functionality into this class
class Search
{
	// @param [String] resource: object, person, exhibition, etc
	// @param [list of Strings] details: which details to display
	constructor(resource, details)
	{
		this.resource = resource;
		this.details = details;
		this.form = document.forms.namedItem(resource + '-form');
		this.resultsJson = {};
	}

	// from https://lengstorf.com/get-form-values-as-json/
	/**
	* Retrieves input data from a form and returns it as a JSON object.
	* @param  {HTMLFormControlsCollection} elements  the form elements
	* @return {Object}                               form data as an object literal
	*/
	formToJSON = elements => [].reduce.call(elements, (data, element) => {

	  data[element.name] = element.value;
	  return data;

	}, {});

	handleFormSubmit = event => {
		event.preventDefault();
		const form_data = this.formToJSON(this.form.elements);
		this.doSearches(form_data);
	}

	async doSearches(form_data)
	{
		if (this.resource == "object")
		{
			this.resultsJson = await objectSearch(form_data);
			this.displayResults();
		}
		else if (this.resource == "person")
		{
			this.resultsJson = await personSearch(form_data);
			this.displayResults();
		}
		else if (this.resource == "exhibition")
		{
			this.resultsJson = await exhibitionSearch(form_data);
			this.displayResults();
		}
		else if (this.resource == "publication")
		{
			this.resultsJson = await publicationSearch(form_data);
			this.displayResults();
		}
	}

	async displayResults()
	{
		// clear old search results
	 	let old_results = document.getElementById(this.resource + "-results");
	 	if (old_results)
	 	{
	 		old_results.parentNode.removeChild(old_results);
	 	}

	 	// display new search results
	 	let results_div = document.createElement("div");
	 	results_div.id = this.resource + "-results";
	 	this.form.appendChild(results_div);

	 	// display result details
	 	if (this.resultsJson.records.length == 0)
	 	{
	 		results_div.innerHTML = "No results";
	 	}
	 	else
	 	{
	 		results_div.innerHTML = "Results: Page " + this.resultsJson.info.page;
	 	}

	 	// list results
	 	for (let n in this.resultsJson.records)
	 	{
	 		let record = this.resultsJson.records[n];
	 		let result = document.createElement("div");
	 		let result_id = "result-" + record.id;
	 		result.id = result_id;
	 		result.style = "border:1px solid black;";
	 		results_div.appendChild(result);

	 		let ul = create_ul(record, this.details);
	 		result.appendChild(ul);

	 		// display images if there are any
	 		let images = display_images(record.images);
	 		result.appendChild(images);

	 		// display link to all details
	 		let details_link = document.createElement("a"); 
	 		details_link.href = `${BASE_URL}details/${this.resource}/${record.id}`;
			details_link.innerHTML = "View All Details";
			result.appendChild(details_link);
	 	}

	 	// display button to view previous page
	 	if (this.resultsJson.info.page > 1)
	 	{
	 		let _self = this;
	 		let prev_button = document.createElement("button");
	 		prev_button.innerHTML = "Previous Page";
	 		prev_button.onclick = async function () {
	 			_self.resultsJson = await async_fetch(_self.resultsJson.info.prev);
	 			_self.displayResults();
	 		}
	 		results_div.appendChild(prev_button);
	 	}
	 	// display button to view next page
	 	if (this.resultsJson.info.page < this.resultsJson.info.pages)
	 	{
	 		let _self = this;
	 		let next_button = document.createElement("button");
	 		next_button.innerHTML = "Next Page";
	 		next_button.onclick = async function () {
	 			// TODO: Next Page doesn't work the first time you click it
	 			_self.resultsJson = await async_fetch(_self.resultsJson.info.next);
	 			_self.displayResults();
	 		}
	 		results_div.appendChild(next_button);
	 	}
	}

	setFormSubmit()
	{
		this.form.addEventListener("submit", this.handleFormSubmit);;
	}

}

// start execution
hide_element("search");
let object_search = new Search("object", ["title", "dated", "classification", "period", "department"]);
object_search.setFormSubmit();
let person_search = new Search("person", ["displayname", "gender", "birthplace"]);
person_search.setFormSubmit();
let exhibition_search = new Search("exhibition", ["title", "description", "begindate", "enddate"]);
exhibition_search.setFormSubmit();
let publication_search = new Search("publication", ["title", "citation", "publicationyear", "format", "shortdescription"]);
publication_search.setFormSubmit();
